# Triplano-Project Template

A ideia deste repositório é criar uma base para os projetos em Unity da Triplano.
Variações do "Basic Template" podem ser criadas em branches voltadas para determinados usos.


<details><summary>MVP Template</summary>

- New Input System

- Odin Inspector

- DOTween

- Basic Font and UI Colors Template

- Save System (Json)

- Basic Main Screen (volume options and Play button)

- (wip) Ultimate Debug Console (with cheats and debug)

- (wip) FEEL Asset

- (wip) Basic Loading System

- (wip) SoundBox 4.0

- (wip) Basic Global Event System

- (wip) Object Pooling System

</details>
